export const WebminSharedConstants = {

  BC_EVENT: {
    SWITCHED: 'switched',
    HEAD_CHANGED: 'newHEAD',
    RESOLUTION_DONE: 'resolution_done'
  }
}
