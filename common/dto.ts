import {P2pCandidate} from 'duniter/app/modules/crawler/lib/sync/p2p/p2p-candidate'

export interface SyncProgress {
  download?: number
  saved?: number
  applied?: number
  sandbox?: number
  peersSync?: number
  sync?: boolean
  error?: string
  p2pData?: P2PData
}

export class P2PData {

  name?: string
  data?: P2PDataDetail
}

export class P2PDataDetail {

  chunkIndex?: number
  nodes?: P2pCandidate[]
  node?: P2pCandidate
}

export interface SyncEnding {
  sync: boolean
  msg?: Error
}
