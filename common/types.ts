import {DBBlock} from 'duniter/app/lib/db/DBBlock'
import {TransactionDTO} from 'duniter/app/lib/dto/TransactionDTO'

export type NodeState = 'READY_FOR_SYNC'
  | 'READY_FOR_START'
  | 'SYNCHRONIZING'
  | 'STARTED'
  | 'UNKNOWN'

export interface WS2PConnectionInfos {
  softVersions: SoftVersions
  level1: WS2PConnectionInfo[]
  level2: WS2PConnectionInfo[]
}

export interface WS2PConnectionInfo {
  pubkey: string
  ws2pid: string
  uid: string
  handle: string
}

export interface WS2PHead {
  message:string
  sig:string
  messageV2?:string
  sigV2?:string
  step?:number
}

export interface SoftVersions {
  software: string
  version: string
  pubkeys: string[]
}

export interface DBBlock {

  version: number
  number: number
  currency: string
  hash: string
  inner_hash: string
  signature: string
  previousHash: string
  issuer: string
  previousIssuer: string
  time: number
  powMin: number
  unitbase: number
  membersCount: number
  issuersCount: number
  issuersFrame: number
  issuersFrameVar: number
  identities: string[]
  joiners: string[]
  actives: string[]
  leavers: string[]
  revoked: string[]
  excluded: string[]
  certifications: string[]
  transactions: TransactionDTO[]
  medianTime: number
  nonce: number
  fork: boolean
  parameters: string
  monetaryMass: number
  dividend: number | null
  UDTime: number
  writtenOn: number
  written_on: string
}