import "reflect-metadata";
import {buildSchema} from 'type-graphql'
import {BigResolver} from '../webmin/graphql/resolvers/BigResolver'
import {BigSubscriber} from '../webmin/graphql/subscribers/BigSubscriber'
import {pubsub} from '../webmin/webmin'
import {printSchema} from 'graphql'
import {strictEqual} from 'assert'

describe('GraphQL schema', () => {

  it('should equal', async () => {
    const schemaObj = await buildSchema({
      resolvers: [BigResolver, BigSubscriber],
      pubSub: pubsub
    })

    strictEqual(printSchema(schemaObj), `type BlockchainEventType {
  bcEvent: String!
  block: BlockType!
}

type BlockTransactionType {
  version: Int!
  currency: String!
  locktime: Int!
  hash: String!
  blockstamp: String!
  blockstampTime: Int!
  issuers: [String!]!
  inputs: [String!]!
  outputs: [String!]!
  unlocks: [String!]!
  signatures: [String!]!
  comment: String!
}

type BlockType {
  version: Int!
  number: Int!
  currency: String!
  hash: String!
  inner_hash: String!
  signature: String!
  previousHash: String
  issuer: String!
  previousIssuer: String
  time: Int!
  powMin: Int!
  unitbase: Int!
  membersCount: Int!
  issuersCount: Int!
  issuersFrame: Int!
  issuersFrameVar: Int!
  identities: [String!]!
  joiners: [String!]!
  actives: [String!]!
  leavers: [String!]!
  revoked: [String!]!
  excluded: [String!]!
  certifications: [String!]!
  transactions: [BlockTransactionType!]!
  medianTime: Int!
  nonce: String!
  parameters: String
  monetaryMass: Int!
  dividend: Int
  UDTime: Int
  writtenOn: Int!
  written_on: String
}

type ChainType {
  blocks: [BlockType!]!
}

type DocumentsType {
  blocks: [BlockType!]!
  identities: [PendingIdentityType!]!
  certifications: [PendingCertificationType!]!
  memberships: [PendingMembershipType!]!
  transactions: [PendingTransactionType!]!
  peers: [PeerType!]!
  ws2pHeads: [WS2PHeadType!]!
  ws2pConnections: [WS2PConnectionType!]!
  ws2pDisconnections: [WS2PDisconnectionType!]!
}

type HistoryOfMemberType {
  stories: [String!]!
}

type NetworkConfAvailableType {
  remoteport: Float!
  remotehost: String
  remoteipv4: String
  remoteipv6: String
  port: Float!
  ipv4: String!
  ipv6: String!
  upnp: Boolean!
}

type P2PDataCandidateType {
  reserved: Boolean
  nbSuccess: Int
  isExcluded: Boolean
  failures: Int
  responseTimes: [Float!]!
  p: PeerType
  hostName: String
  hasAvailableApi: Boolean
  apiName: String
  avgResponseTime: Float
}

type P2PDataDetailType {
  chunkIndex: Int
  nodes: [P2PDataCandidateType!]
  node: P2PDataCandidateType
}

type P2PDataType {
  name: String
  data: P2PDataDetailType
}

type PeerType {
  hash: String!
  pubkey: String!
  block: String!
  endpoints: [String!]!
}

type PendingCertificationType {
  linked: Boolean!
  written: Boolean!
  written_block: Int
  written_hash: String
  sig: String!
  block_number: Int!
  block_hash: String!
  target: String
  to: String!
  from: String!
  block: Int!
  expired: Boolean
  expires_on: Int!
}

type PendingIdentityType {
  revoked: Boolean!
  buid: String!
  member: Boolean!
  kick: Boolean!
  leaving: Boolean
  wasMember: Boolean!
  pubkey: String!
  uid: String!
  sig: String!
  revocation_sig: String
  hash: String!
  written: Boolean!
  revoked_on: Int
  expires_on: Int!
  certs: [PendingCertificationType!]!
  memberships: [PendingMembershipType!]!
}

type PendingMembershipType {
  membership: String!
  issuer: String!
  number: Int!
  blockNumber: Int!
  blockHash: String!
  userid: String!
  certts: String!
  block: String
  fpr: String!
  idtyHash: String!
  written: Boolean!
  written_number: Int
  expires_on: Int!
  signature: String!
  expired: Boolean
  block_number: Int
}

type PendingTransactionType {
  hash: String!
  block_number: Int
  locktime: Int!
  version: Int!
  currency: String!
  comment: String!
  blockstamp: String!
  blockstampTime: Int
  time: Int
  inputs: [String!]!
  unlocks: [String!]!
  outputs: [String!]!
  issuers: [String!]!
  signatures: [String!]!
  written: Boolean
  removed: Boolean
  received: Int
  output_base: Int!
  output_amount: Int!
  written_on: String
  writtenOn: Int
}

type Query {
  hello: String
  nodeState: String
  current: BlockType
  heads: [WS2PHeadType!]!
  ws2pinfos: WS2PInfosType!
  isSyncStarted: Boolean!
  stopAndResetData: Boolean!
  startNode: Boolean!
  synchronize(url: String!): Boolean
  uid(pub: String!): String
  getConf: String!
  testAndSaveConf(conf: String!): String!
  getMainChain(end: Int!, start: Int!): ChainType!
  historyOfMember(pub: String!): HistoryOfMemberType!
  getForks(end: Int!, start: Int!): ChainType!
  getAvailableNetworkConf: NetworkConfAvailableType!
}

type SoftVersionType {
  software: String!
  version: String!
  pubkeys: [String!]!
}

type Subscription {
  syncProgress: SyncProgressType
  newDocuments: DocumentsType
  bcEvents: BlockchainEventType
}

type SyncProgressType {
  download: Float
  saved: Float
  applied: Float
  sandbox: Float
  peersSync: Float
  sync: Boolean
  p2pData: P2PDataType
  error: String
}

type WS2PConnectionInfoType {
  pubkey: String
  ws2pid: String
  uid: String
  handle: String
}

type WS2PConnectionType {
  host: String!
  port: String!
  pub: String!
}

type WS2PDisconnectionType {
  pub: String
}

type WS2PHeadType {
  message: String!
  sig: String!
  messageV2: String
  sigV2: String
  step: Int
}

type WS2PInfosType {
  softVersions: [SoftVersionType!]!
  level1: [WS2PConnectionInfoType!]!
  level2: [WS2PConnectionInfoType!]!
}
`)
  })
})