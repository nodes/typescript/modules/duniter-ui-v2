import {Duniter} from 'duniter';
import {webminHttpListen} from './webmin/network';
import {BackendConstants} from './constants';

Duniter.run([{
  name: 'duiv2',
  required: {
    duniter: {

      cliOptions: [

        // Webmin options
        { value: '--webmhost <host>', desc: 'Local network interface to connect to (IP)' },
        { value: '--webmport <port>', desc: 'Local network port to connect', parser: parseInt }
      ],

      cli: [{
        name: 'ui',
        desc: 'Starts the UI backend',
        onDatabaseExecute: async (server, conf, program: any, params, start, stop) => {

          const host = program.webmhost || BackendConstants.WEBMIN_DEFAULT_HOST
          const port = program.webmport || BackendConstants.WEBMIN_DEFAULT_PORT

          // Never ending promise
          await new Promise(async () => {
            await webminHttpListen(server, port, host, start, stop)
          })
        }
      }]
    }
  }
}])
