import * as http from 'http';
import {createServer} from 'http';
import * as express from 'express';
import {Server} from 'duniter/server';
import {graphiqlExpress, graphqlExpress,} from 'graphql-server-express';
import {plugModule} from './webmin';
import {execute, subscribe} from 'graphql';
import * as path from 'path'
import {SubscriptionServer} from 'subscriptions-transport-ws'
import bodyParser = require('body-parser')
import cors = require('cors')

export async function webminHttpListen(
  server: Server,
  port: number,
  host = 'localhost',
  startServices: () => Promise<void>,
  stopServices: () => Promise<void>,
): Promise<http.Server> {

  const schema = await plugModule(server, startServices, stopServices)

  const app = (express as any)()

  app.use('*', cors({ origin: new RegExp(`${host}`) }))

  app.use('/graphql', bodyParser.json(), graphqlExpress({
    schema
  }))

  app.use('/graphiql', graphiqlExpress({
    endpointURL: '/graphql',
    subscriptionsEndpoint: `ws://${host}:${port}/subscriptions`
  }))

  const front = path.join(__dirname, '../..', 'front/dist/front')
  app.use(express.static(front))

  // Wrap the Express server
  const ws = createServer(app);
  return ws.listen(port, () => {
    console.log(`DuniterUI Server is now running on http://${host}:${port}`)
    // Set up the WebSocket for handling GraphQL subscriptions
    new SubscriptionServer({
      execute,
      subscribe,
      schema
    }, {
      server: ws,
      path: '/subscriptions',
    })
  })
}
