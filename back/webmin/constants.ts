export const GraphQLSubscriptions = {

  SYNC_PROGRESS: 'sync_progress',
  NEW_DOCUMENTS: 'new_documents',
  BC_EVENTS: 'bc_events',

}