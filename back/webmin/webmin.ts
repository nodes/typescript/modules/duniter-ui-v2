import "reflect-metadata";
import {Server} from 'duniter/server';

import {PubSub} from 'graphql-subscriptions';
import {buildSchema} from 'type-graphql'
import {BigResolver} from './graphql/resolvers/BigResolver'
import {ApplicationContext} from './graphql/di/application-context'
import {BigSubscriber} from './graphql/subscribers/BigSubscriber'
import {gqlSubscribeBlockchainEvents} from './subscriptions/gql-events-blockchain'
import {gqlSubscribeDocuments} from './subscriptions/gql-events-documents'

export const pubsub = new PubSub();

export function plugModule(server: Server, startServices: () => Promise<void>, stopServices: () => Promise<void>) {

  // Dependency Injection
  ApplicationContext.server = server
  ApplicationContext.startServices = startServices
  ApplicationContext.stopServices = stopServices

  gqlSubscribeBlockchainEvents(server)
  gqlSubscribeDocuments(server)

  return buildSchema({
    resolvers: [BigResolver, BigSubscriber],
    pubSub: pubsub
  })
}
