import {GraphQLSubscriptions} from '../constants'
import {pubsub} from '../webmin'
import {SyncProgress} from '../../../common/dto'
import {transformSyncProgress} from '../graphql/types/transform/sync-progress.transform'

export function gqlPushSyncProgress(progress: SyncProgress) {
  return pubsub.publish(GraphQLSubscriptions.SYNC_PROGRESS, transformSyncProgress(progress))
}
