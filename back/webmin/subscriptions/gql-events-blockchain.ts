import {Server} from 'duniter/server'
import {GraphQLSubscriptions} from '../constants'
import {pubsub} from '../webmin'

export function gqlSubscribeBlockchainEvents(server: Server) {

  server.on('data', async (data: any) => {
    if (data.bcEvent) {
      return pubsub.publish(GraphQLSubscriptions.BC_EVENTS, data)
    }
  })
}
