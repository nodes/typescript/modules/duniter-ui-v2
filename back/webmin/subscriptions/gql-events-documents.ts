import {Server} from 'duniter/server'
import {softVersions} from '../shared/gql-network'
import {GraphQLSubscriptions} from '../constants'
import {pubsub} from '../webmin'

export function gqlSubscribeDocuments(server: Server) {

  server.on('data', async (data: any) => {

    const newDocuments = {
      blocks: [] as any[],
      identities: [] as any[],
      certifications: [] as any[],
      memberships: [] as any[],
      transactions: [] as any[],
      peers: [] as any[],
      ws2pHeads: [] as any[],
      ws2pConnections: [] as any[],
      ws2pDisconnections: [] as any[],
    }
    if (data.joiners) {
      newDocuments.blocks.push(data)
    }
    else if (data.ws2p === 'heads') {
      data.added.forEach((h: any) => {
        // Trace all the versions of all WS2P heads
        const [conf, type, version, pub, blockstamp, uuid, soft, softV, room1, room2] = h.message.split(':')
        const softObj = softVersions[soft] || (softVersions[soft] = {})
        if (!softVersions[soft]) {
          softVersions[soft] = {}
        }
        const softVer = softObj[softV] || (softObj[softV] = [])
        if (!softVer.includes(pub)) {
          softVer.push(pub)
        }
        newDocuments.ws2pHeads.push(h)
      })
    }
    else if (data.ws2p === 'connected') {
      newDocuments.ws2pConnections.push({
        host: data.to.host,
        port: data.to.port,
        pub: data.to.pubkey,
      })
    }
    else if (data.ws2p === 'disconnected') {
      newDocuments.ws2pDisconnections.push({
        pub: data.peer.pub,
      })
    }
    else if (data.endpoints) {
      newDocuments.peers.push(data)
    }
    else if (data.inputs) {
      newDocuments.transactions.push(data)
    }
    else if (data.membership) {
      newDocuments.memberships.push(data)
    }
    else if (data.from) {
      newDocuments.certifications.push(data)
    }
    else if (data.uid) {
      newDocuments.identities.push(data)
    }

    await pubsub.publish(GraphQLSubscriptions.NEW_DOCUMENTS, newDocuments)
  })
}
