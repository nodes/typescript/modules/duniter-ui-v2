export const softVersions: {
  [software: string]: {
    [version: string]: string[]
  }
} = {}
