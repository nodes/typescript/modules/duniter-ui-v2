import {BlockType} from '../types/BlockType'
import {WS2PHeadType} from '../types/WS2PHeadType'
import {WS2PInfosType} from '../types/WS2PInfosType'
import {Resolver} from 'type-graphql/dist/decorators/Resolver'
import {Int, Query} from 'type-graphql'
import {gqlNodeStart, gqlNodeState, gqlStopAndResetData} from '../../queries/gql-node-state'
import {ApplicationContext} from '../di/application-context'
import {gqlCurrent} from '../../queries/gql-current'
import {gqlHeads} from '../../queries/gql-heads'
import {gqlWs2pInfos} from '../../queries/gql-ws2p'
import {gqlIsSyncStarted, gqlSynchronize} from '../../queries/gql-synchronize'
import {gqlUid} from '../../queries/gql-uid'
import {getConf, getNetworkConfigurations, testAndSaveConf} from '../../queries/gql-conf'
import {NetworkConfAvailableType} from '../types/NetworkConfAvailalbeType'
import {Arg} from 'type-graphql/dist/decorators/Arg'
import {getForks, getMainChain} from '../../queries/gql-forks'
import {ChainType} from '../types/ChainType'
import {HistoryOfMemberType} from '../types/HistoryOfMemberType'
import {getHistoryOfMember} from '../../queries/gql-data'

@Resolver()
export class BigResolver {

  @Query({ nullable: true })
  hello(): string {
    return 'Hello from Duniter Web Admin API'
  }

  @Query(type => String, { nullable: true })
  async nodeState() {
    return await gqlNodeState(ApplicationContext.server)()
  }

  @Query(type => BlockType, { nullable: true })
  async current() {
    return await gqlCurrent(ApplicationContext.server)()
  }

  @Query(type => [WS2PHeadType])
  async heads(): Promise<WS2PHeadType[]> {
    return gqlHeads(ApplicationContext.server)()
  }

  @Query(type => WS2PInfosType)
  ws2pinfos(): Promise<WS2PInfosType> {
    return gqlWs2pInfos(ApplicationContext.server)()
  }

  @Query(type => Boolean)
  isSyncStarted() {
    return gqlIsSyncStarted()()
  }

  @Query(type => Boolean)
  stopAndResetData(): Promise<boolean> {
    return gqlStopAndResetData(ApplicationContext.server, ApplicationContext.stopServices)()
  }

  @Query(type => Boolean)
  startNode(): Promise<boolean> {
    return gqlNodeStart(ApplicationContext.startServices)()
  }

  @Query(type => Boolean, { nullable: true })
  synchronize(@Arg("url") url: string) {
    return gqlSynchronize(ApplicationContext.server)(null, { url })
  }

  @Query(type => String, { nullable: true })
  uid(@Arg("pub") pub: string): Promise<string|null> {
    return gqlUid(ApplicationContext.server)(null, { pub })
  }

  @Query(type => String)
  getConf(): Promise<string> {
    return getConf(ApplicationContext.server)
  }

  @Query(type => String)
  testAndSaveConf(@Arg("conf") conf: string): Promise<string> {
    return testAndSaveConf(ApplicationContext.server, ApplicationContext.startServices, ApplicationContext.stopServices, conf)
  }

  @Query(type => ChainType)
  getMainChain(@Arg("start", type => Int) start: number, @Arg("end", type => Int) end: number): Promise<ChainType> {
    return getMainChain(ApplicationContext.server, start, end)
  }

  @Query(type => HistoryOfMemberType)
  historyOfMember(@Arg("pub", type => String) pub: string): Promise<HistoryOfMemberType> {
    return getHistoryOfMember(ApplicationContext.server, pub)
  }

  @Query(type => ChainType)
  getForks(@Arg("start", type => Int) start: number, @Arg("end", type => Int) end: number): Promise<ChainType> {
    return getForks(ApplicationContext.server, start, end)
  }

  @Query(type => NetworkConfAvailableType)
  getAvailableNetworkConf(): Promise<NetworkConfAvailableType> {
    return getNetworkConfigurations(ApplicationContext.server)
  }
}
