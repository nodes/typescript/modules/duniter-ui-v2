import {Server} from 'duniter/server'

class LocalApplicationContext {

  private _server: Server
  private _startServices: () => Promise<void>
  private _stopServices: () => Promise<void>

  set server(value: Server) {
    this._server = value
  }

  get server(): Server {
    return this._server
  }


  get startServices(): () => Promise<void> {
    return this._startServices
  }

  set startServices(value: () => Promise<void>) {
    this._startServices = value
  }
  get stopServices(): () => Promise<void> {
    return this._stopServices
  }

  set stopServices(value: () => Promise<void>) {
    this._stopServices = value
  }
}

export const ApplicationContext = new LocalApplicationContext()