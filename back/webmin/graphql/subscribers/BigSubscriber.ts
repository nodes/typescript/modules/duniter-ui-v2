import {Root, Subscription} from 'type-graphql'
import {GraphQLSubscriptions} from '../../constants'
import {SyncProgressType} from '../types/SyncProgressType'
import {DocumentsType} from '../types/DocumentsType'
import {BlockchainEventType} from '../types/BlockchainEventType'

export class BigSubscriber {

  @Subscription(type => SyncProgressType, { nullable: true, topics: GraphQLSubscriptions.SYNC_PROGRESS })
  syncProgress(@Root() progress: SyncProgressType): SyncProgressType {
    return progress
  }

  @Subscription(type => DocumentsType, { nullable: true, topics: GraphQLSubscriptions.NEW_DOCUMENTS })
  newDocuments(@Root() documents: DocumentsType): DocumentsType {
    return documents
  }

  @Subscription(type => BlockchainEventType, { nullable: true, topics: GraphQLSubscriptions.BC_EVENTS })
  bcEvents(@Root() bcEvents: BlockchainEventType): BlockchainEventType {
    return bcEvents
  }
}
