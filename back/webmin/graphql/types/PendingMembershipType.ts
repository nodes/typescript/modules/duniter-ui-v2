import {Field, Int, ObjectType} from 'type-graphql'

@ObjectType()
export class PendingMembershipType {
  @Field() membership?: string
  @Field() issuer?: string
  @Field(type => Int) number: number
  @Field(type => Int) blockNumber: number
  @Field() blockHash?: string
  @Field() userid?: string
  @Field() certts?: string
  @Field({ nullable: true }) block?: string
  @Field() fpr?: string
  @Field() idtyHash?: string
  @Field() written?: boolean
  @Field(type => Int, { nullable: true }) written_number?: number
  @Field(type => Int) expires_on: number
  @Field() signature: string
  @Field({ nullable: true }) expired?: boolean
  @Field(type => Int, { nullable: true }) block_number?: number
}
