import {SyncProgress} from '../../../../../common/dto'
import {P2PDataDetailType} from '../P2PDataDetailType'
import {SyncProgressType} from '../SyncProgressType'
import {transformP2pCandidate} from './p2p-candidate.transform'


/**
 * Transforms the `progress` object from Duniter to our GraphQL type
 * @param progress
 */
export function transformSyncProgress(progress: SyncProgress): SyncProgressType {
  const p: SyncProgressType = {
    download: progress.download,
    saved: progress.saved,
    applied: progress.applied,
    sandbox: progress.sandbox,
    peersSync: progress.peersSync,
    sync: progress.sync,
    error: progress.error,
  }
  if (progress.p2pData && progress.p2pData.data) {
    const data: P2PDataDetailType = {
      chunkIndex: progress.p2pData.data.chunkIndex
    }
    if (progress.p2pData.data.node) {
      data.node = transformP2pCandidate(progress.p2pData.data.node)
    }
    if (progress.p2pData.data.nodes) {
      data.nodes = progress.p2pData.data.nodes.map(transformP2pCandidate)
    }
    p.p2pData = {
      name: progress.p2pData.name,
      data
    }
  }
  return p
}