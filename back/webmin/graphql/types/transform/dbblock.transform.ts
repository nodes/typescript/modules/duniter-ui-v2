import {DBBlock} from 'duniter/app/lib/db/DBBlock'
import {BlockType} from '../BlockType'


export function transformDBBlock(b: DBBlock): BlockType {
  return {
    version: b.version,
    number: b.number,
    currency: b.currency,
    hash: b.hash,
    inner_hash: b.inner_hash,
    signature: b.signature,
    previousHash: b.previousHash,
    issuer: b.issuer,
    previousIssuer: b.previousIssuer,
    time: b.time,
    powMin: b.powMin,
    unitbase: b.unitbase,
    membersCount: b.membersCount,
    issuersCount: b.issuersCount,
    issuersFrame: b.issuersFrame,
    issuersFrameVar: b.issuersFrameVar,
    identities: b.identities,
    joiners: b.joiners,
    actives: b.actives,
    leavers: b.leavers,
    revoked: b.revoked,
    excluded: b.excluded,
    certifications: b.certifications,
    transactions: b.transactions,
    medianTime: b.medianTime,
    nonce: b.nonce,
    parameters: b.parameters,
    monetaryMass: b.monetaryMass,
    dividend: b.dividend || undefined,
    UDTime: b.UDTime,
    writtenOn: b.writtenOn,
    written_on: b.written_on,
  }
}
