import {P2pCandidate} from 'duniter/app/modules/crawler/lib/sync/p2p/p2p-candidate'
import {P2PDataCandidateType} from '../P2PDataCandidateType'


export function transformP2pCandidate(node: P2pCandidate): P2PDataCandidateType {
  return {
    reserved: (node as any).reserved,
    nbSuccess: (node as any).nbSuccess,
    isExcluded: (node as any).isExcluded,
    failures: (node as any).failures,
    responseTimes: (node as any).responseTimes,
    p: node.p,
    hostName: node.hostName,
    hasAvailableApi: node.hasAvailableApi(),
    apiName: node.apiName || undefined,
    avgResponseTime: node.avgResponseTime(),
  };
}
