import {Field, ObjectType} from 'type-graphql'

@ObjectType()
export class PeerType {

  @Field() hash?: string
  @Field() pubkey?: string
  @Field() block?: string

  @Field(type => [String])
  endpoints?: string[]
}
