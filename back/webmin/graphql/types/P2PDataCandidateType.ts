import {Field, Float, Int, ObjectType} from 'type-graphql'
import {PeerType} from './PeerType'

@ObjectType()
export class P2PDataCandidateType {

  @Field({ nullable: true }) reserved?: boolean
  @Field(type => Int, { nullable: true }) nbSuccess?: number
  @Field({ nullable: true }) isExcluded?: boolean
  @Field(type => Int, { nullable: true }) failures?: number
  @Field(type => [Float]) responseTimes?: number[]
  @Field({ nullable: true }) p?: PeerType
  @Field({ nullable: true }) hostName?: string
  @Field({ nullable: true }) hasAvailableApi?: boolean
  @Field({ nullable: true }) apiName?: string
  @Field({ nullable: true }) avgResponseTime?: number
}