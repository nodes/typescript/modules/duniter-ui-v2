import {Field, ObjectType} from 'type-graphql'

@ObjectType()
export class WS2PConnectionType {

  @Field() host?: string
  @Field() port?: string
  @Field() pub?: string
}