import {Field, Float, ObjectType} from 'type-graphql'
import {P2PDataType} from './P2PDataType'

@ObjectType()
export class SyncProgressType {

  @Field(type => Float, { nullable: true }) download?: number
  @Field(type => Float, { nullable: true }) saved?: number
  @Field(type => Float, { nullable: true }) applied?: number
  @Field(type => Float, { nullable: true }) sandbox?: number
  @Field(type => Float, { nullable: true }) peersSync?: number
  @Field({ nullable: true }) sync?: boolean
  @Field({ nullable: true }) p2pData?: P2PDataType
  @Field({ nullable: true }) error?: string
}
