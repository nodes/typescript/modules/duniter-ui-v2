import {Field, Int, ObjectType} from 'type-graphql'
import {BlockTransactionType} from './BlockTransactionType'

@ObjectType()
export class BlockType {
  @Field(type => Int) version: number
  @Field(type => Int) number: number
  @Field() currency: string
  @Field() hash: string
  @Field() inner_hash: string
  @Field() signature: string
  @Field({ nullable: true }) previousHash?: string
  @Field() issuer: string
  @Field({ nullable: true }) previousIssuer?: string
  @Field(type => Int) time: number
  @Field(type => Int) powMin: number
  @Field(type => Int) unitbase: number
  @Field(type => Int) membersCount: number
  @Field(type => Int) issuersCount: number
  @Field(type => Int) issuersFrame: number
  @Field(type => Int) issuersFrameVar: number
  @Field(type => [String]) identities: string[]
  @Field(type => [String]) joiners: string[]
  @Field(type => [String]) actives: string[]
  @Field(type => [String]) leavers: string[]
  @Field(type => [String]) revoked: string[]
  @Field(type => [String]) excluded: string[]
  @Field(type => [String]) certifications: string[]
  @Field(type => [BlockTransactionType]) transactions: BlockTransactionType[]
  @Field(type => Int) medianTime: number
  @Field(type => String) nonce?: number
  @Field({ nullable: true }) parameters?: string
  @Field(type => Int) monetaryMass: number
  @Field(type => Int, { nullable: true }) dividend?: number
  @Field(type => Int, { nullable: true }) UDTime?: number
  @Field(type => Int) writtenOn: number
  @Field({ nullable: true }) written_on?: string
}
