import {Field, ObjectType} from 'type-graphql'

@ObjectType()
export class WS2PConnectionInfoType {

  @Field({ nullable: true }) pubkey?: string
  @Field({ nullable: true }) ws2pid?: string
  @Field({ nullable: true }) uid?: string
  @Field({ nullable: true }) handle?: string
}