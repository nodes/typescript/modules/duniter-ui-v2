import {Field, ObjectType} from 'type-graphql'

@ObjectType()
export class SoftVersionType {

  @Field() software?: string
  @Field() version?: string
  @Field(type => [String]) pubkeys: string[]
}