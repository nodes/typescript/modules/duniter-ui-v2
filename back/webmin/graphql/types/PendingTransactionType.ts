import {Field, Int, ObjectType} from 'type-graphql'

@ObjectType()
export class PendingTransactionType {
  @Field() hash?: string
  @Field(type => Int, { nullable: true }) block_number?: number
  @Field(type => Int) locktime: number
  @Field(type => Int) version: number
  @Field() currency?: string
  @Field() comment?: string
  @Field() blockstamp?: string
  @Field(type => Int, { nullable: true }) blockstampTime?: number
  @Field(type => Int, { nullable: true }) time?: number
  @Field(type => [String]) inputs: string[]
  @Field(type => [String]) unlocks: string[]
  @Field(type => [String]) outputs: string[]
  @Field(type => [String]) issuers: string[]
  @Field(type => [String]) signatures: string[]
  @Field({ nullable: true }) written?: boolean
  @Field({ nullable: true }) removed?: boolean
  @Field(type => Int, { nullable: true }) received?: number
  @Field(type => Int) output_base: number
  @Field(type => Int) output_amount: number
  @Field({ nullable: true }) written_on?: string
  @Field(type => Int, { nullable: true }) writtenOn?: number
}
