import {Field, Int, ObjectType} from 'type-graphql'
import {P2PDataCandidateType} from './P2PDataCandidateType'

@ObjectType()
export class P2PDataDetailType {

  @Field(type => Int, { nullable: true }) chunkIndex?: number
  @Field(type => [P2PDataCandidateType], { nullable: true }) nodes?: P2PDataCandidateType[]
  @Field({ nullable: true }) node?: P2PDataCandidateType
}