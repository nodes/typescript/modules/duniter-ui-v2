import {Field, Int, ObjectType} from 'type-graphql'

@ObjectType()
export class PendingCertificationType {

  @Field() linked?: boolean
  @Field() written?: boolean
  @Field(type => Int, { nullable: true }) written_block?: number
  @Field({ nullable: true }) written_hash?: string
  @Field() sig?: string
  @Field(type => Int) block_number: number
  @Field() block_hash?: string
  @Field({ nullable: true }) target?: string
  @Field() to?: string
  @Field() from?: string
  @Field(type => Int) block: number
  @Field({ nullable: true }) expired?: boolean
  @Field(type => Int) expires_on: number
}
