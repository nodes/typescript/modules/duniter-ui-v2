import {Field, ObjectType} from 'type-graphql'
import {P2PDataDetailType} from './P2PDataDetailType'

@ObjectType()
export class P2PDataType {

  @Field({ nullable: true }) name?: string
  @Field({ nullable: true }) data?: P2PDataDetailType
}