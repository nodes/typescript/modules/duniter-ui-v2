import {ObjectType} from 'type-graphql/dist/decorators/ObjectType'
import {Field, Int} from 'type-graphql'

@ObjectType()
export class WS2PHeadType {

  @Field() message?: string
  @Field() sig?: string
  @Field({ nullable: true }) messageV2?: string
  @Field({ nullable: true }) sigV2?: string
  @Field(type => Int, { nullable: true }) step?: number
}
