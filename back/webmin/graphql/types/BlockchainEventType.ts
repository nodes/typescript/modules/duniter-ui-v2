import {Field, ObjectType} from 'type-graphql'
import {BlockType} from './BlockType'

@ObjectType()
export class BlockchainEventType {

  @Field() bcEvent?: string
  @Field() block: BlockType
}
