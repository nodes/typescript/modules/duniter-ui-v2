import {Field, Int, ObjectType} from 'type-graphql'
import {PendingCertificationType} from './PendingCertificationType'
import {PendingMembershipType} from './PendingMembershipType'

@ObjectType()
export class PendingIdentityType {
  @Field() revoked?: boolean
  @Field() buid?: string
  @Field() member?: boolean
  @Field() kick?: boolean
  @Field({ nullable: true }) leaving?: boolean
  @Field() wasMember?: boolean
  @Field() pubkey?: string
  @Field() uid?: string
  @Field() sig?: string
  @Field({ nullable: true }) revocation_sig?: string
  @Field() hash?: string
  @Field() written?: boolean
  @Field(type => Int, { nullable: true }) revoked_on?: number
  @Field(type => Int) expires_on: number
  @Field(type => [PendingCertificationType]) certs: PendingCertificationType[]
  @Field(type => [PendingMembershipType]) memberships: PendingMembershipType[]
}
