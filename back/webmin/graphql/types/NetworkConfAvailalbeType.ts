import {Field, ObjectType} from 'type-graphql'

@ObjectType()
export class NetworkConfAvailableType {

  @Field() remoteport: number
  @Field(type => String, { nullable: true }) remotehost: string|null
  @Field(type => String, { nullable: true }) remoteipv4: string|null
  @Field(type => String, { nullable: true }) remoteipv6: string|null
  @Field() port: number
  @Field() ipv4: string
  @Field() ipv6: string
  @Field() upnp: boolean
}
