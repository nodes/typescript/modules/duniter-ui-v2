import {Field, ObjectType} from 'type-graphql'

@ObjectType()
export class WS2PDisconnectionType {
  @Field({ nullable: true }) pub?: string
}
