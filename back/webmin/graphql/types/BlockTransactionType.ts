import {Int, ObjectType} from 'type-graphql'
import {Field} from 'type-graphql/dist/decorators/Field'

@ObjectType()
export class BlockTransactionType {

  @Field(type => Int) version: number
  @Field() currency?: string
  @Field(type => Int) locktime: number
  @Field() hash?: string
  @Field() blockstamp?: string
  @Field(type => Int) blockstampTime: number
  @Field(type => [String]) issuers: string[]
  @Field(type => [String]) inputs: string[]
  @Field(type => [String]) outputs: string[]
  @Field(type => [String]) unlocks: string[]
  @Field(type => [String]) signatures: string[]
  @Field() comment?: string
}
