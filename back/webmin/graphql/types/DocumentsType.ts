import {Field, ObjectType} from 'type-graphql'
import {WS2PDisconnectionType} from './WS2PDisconnectionType'
import {WS2PConnectionType} from './WS2PConnectionType'
import {WS2PHeadType} from './WS2PHeadType'
import {PeerType} from './PeerType'
import {BlockType} from './BlockType'
import {PendingIdentityType} from './PendingIdentityType'
import {PendingCertificationType} from './PendingCertificationType'
import {PendingMembershipType} from './PendingMembershipType'
import {PendingTransactionType} from './PendingTransactionType'

@ObjectType()
export class DocumentsType {

  @Field(type => [BlockType]) blocks: BlockType[]
  @Field(type => [PendingIdentityType]) identities: PendingIdentityType[]
  @Field(type => [PendingCertificationType]) certifications: PendingCertificationType[]
  @Field(type => [PendingMembershipType]) memberships: PendingMembershipType[]
  @Field(type => [PendingTransactionType]) transactions: PendingTransactionType[]
  @Field(type => [PeerType]) peers: PeerType[]
  @Field(type => [WS2PHeadType]) ws2pHeads: WS2PHeadType[]
  @Field(type => [WS2PConnectionType]) ws2pConnections: WS2PConnectionType[]
  @Field(type => [WS2PDisconnectionType]) ws2pDisconnections: WS2PDisconnectionType[]
}