import {ObjectType} from 'type-graphql'
import {Field} from 'type-graphql/dist/decorators/Field'
import {WS2PConnectionInfoType} from './WS2PConnectionInfoType'
import {SoftVersionType} from './SoftVersionType'

@ObjectType()
export class WS2PInfosType {

  @Field(type => [SoftVersionType]) softVersions: SoftVersionType[]
  @Field(type => [WS2PConnectionInfoType]) level1: WS2PConnectionInfoType[]
  @Field(type => [WS2PConnectionInfoType]) level2: WS2PConnectionInfoType[]
}
