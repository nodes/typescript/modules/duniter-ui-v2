import {Field, Int, ObjectType} from 'type-graphql'
import {BlockTransactionType} from './BlockTransactionType'
import {BlockType} from './BlockType'

@ObjectType()
export class HistoryOfMemberType {

  @Field(type => [String]) stories: string[]
}
