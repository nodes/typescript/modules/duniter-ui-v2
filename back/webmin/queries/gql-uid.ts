import {Server} from 'duniter/server'

export function gqlUid(server: Server) {
  return async (_: any, params: { pub: string }) => {
    const idty = await server.dal.getWrittenIdtyByPubkey(params.pub)
    if (idty) {
      return idty.uid
    }
    return null
  }
}
