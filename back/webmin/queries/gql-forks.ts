import {Server} from 'duniter/server'
import {transformDBBlock} from '../graphql/types/transform/dbblock.transform'
import {ChainType} from '../graphql/types/ChainType'

export async function getMainChain(server: Server, start: number, end: number): Promise<ChainType> {
  const current = await server.dal.getCurrentBlockOrNull()
  if (!current) {
    return { blocks: [] }
  }
  const main = await server.dal.getBlocksBetween(start, end)
  return { blocks: main.map(transformDBBlock) }
}

export async function getForks(server: Server, start: number, end: number): Promise<ChainType> {
  const current = await server.dal.getCurrentBlockOrNull()
  if (!current) {
    return { blocks: [] }
  }
  const main = await server.dal.getPotentialForkBlocks(start, 0, end)
  return { blocks: main.map(transformDBBlock) }
}
