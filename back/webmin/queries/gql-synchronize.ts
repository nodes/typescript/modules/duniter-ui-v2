import {Server} from 'duniter/server'
import {Querable, querablep} from 'duniter/app/lib/common-libs/querable'
import {CrawlerDependency} from 'duniter/app/modules/crawler'
import {SyncEnding, SyncProgress} from '../../../common/dto'
import {gqlPushSyncProgress} from '../subscriptions/gql-events-sync-progress'

let syncPromise: Querable<void> = querablep(Promise.resolve())

export function getSyncPromise() {
  return syncPromise
}

export function gqlIsSyncStarted() {
  return () => !syncPromise.isFulfilled()
}

export function gqlSynchronize(server: Server) {
  return async (_: any, { url }: { url: string }) => {
    // Wait for (eventual) last sync to end
    try {
      await syncPromise
    } catch (e) {
      console.error(e)
    }

    // Reset all the node's data
    await server.dal.close()
    await server.resetData()
    await server.dal.init(server.conf)

    // Begin sync
    const host = url.split(':')[0]
    const port = url.split(':')[1]
    const sync = CrawlerDependency.duniter.methods.synchronize(server, host, parseInt(port), undefined as any, 250)

    // Publish the flow of synchronization for the UI
    sync.flow.on('data', async (syncProgress: SyncEnding|SyncProgress) => {
      if (syncProgress.sync !== undefined) {
        const ending = syncProgress as SyncEnding
        await gqlPushSyncProgress({
          error: ending.msg && ending.msg.message
        })
      }
      await gqlPushSyncProgress(syncProgress)
    })

    syncPromise = querablep(sync.syncPromise)
  }
}
