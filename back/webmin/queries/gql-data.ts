import {Server} from 'duniter/server'
import {HistoryOfMemberType} from '../graphql/types/HistoryOfMemberType'
import {CertificationDTO} from 'duniter/app/lib/dto/CertificationDTO'
import {DBBlock} from 'duniter/app/lib/db/DBBlock'

export async function getHistoryOfMember(server: Server, pub: string): Promise<HistoryOfMemberType> {
  const stories = []
  const start = Date.now()
  const pubRegexp = new RegExp(pub)
  const blocks = await Promise.all((await server.dal.blockDAL.findWithIdentities())
    .concat(await server.dal.blockDAL.findWithCertifications())
    .concat(await server.dal.blockDAL.findWithJoiners())
    .concat(await server.dal.blockDAL.findWithActives())
    .concat(await server.dal.blockDAL.findWithLeavers())
    .concat(await server.dal.blockDAL.findWithExcluded())
    .concat(await server.dal.blockDAL.findWithRevoked())
    .map(async bNumber => (await server.dal.blockDAL.getBlock(bNumber)) as DBBlock))

  blocks.forEach(b => {

    const fNumber = String(b.number).padStart(6, '0')

    b.identities
      .filter(i => i.match(pubRegexp))
      .forEach(i => stories.push(`b#${fNumber}: IDENTITY`))

    const certifies = b.certifications
      .map(CertificationDTO.fromInline)
      .filter(c => c.from === pub)

    if (certifies.length) {
      stories.push(`b#${fNumber}: CERTIFIES ${certifies.length} member(s)`)
    }

    const certified = b.certifications
      .map(CertificationDTO.fromInline)
      .filter(c => c.to === pub)

    if (certified.length) {
      stories.push(`b#${fNumber}: CERTIFIED BY ${certified.length} member(s)`)
    }

    b.joiners
      .filter(i => i.match(pubRegexp))
      .forEach(i => stories.push(
        `b#${fNumber}: JOIN`)
      )

    b.actives
      .filter(i => i.match(pubRegexp))
      .forEach(i => stories.push(
        `b#${fNumber}: RENEW`)
      )

    b.leavers
      .filter(i => i.match(pubRegexp))
      .forEach(i => stories.push(
        `b#${fNumber}: LEAVE`)
      )

    b.revoked
      .filter(i => i.match(pubRegexp))
      .forEach(i => stories.push(
        `b#${fNumber}: REVOKED`)
      )

  })
  stories.push(`scanned all the blocs in ${Date.now() - start}ms`)
  return { stories }
}