import {Server} from 'duniter/server'
import {WS2PConnection} from 'duniter/app/modules/ws2p/lib/WS2PConnection'
import {softVersions} from '../shared/gql-network'
import {WS2PInfosType} from '../graphql/types/WS2PInfosType'
import {WS2PConnectionInfoType} from '../graphql/types/WS2PConnectionInfoType'

export function gqlWs2pInfos(server: Server) {
  return async (): Promise<WS2PInfosType> => {
    if (server.ws2pCluster) {
      let level1 = await server.ws2pCluster.getLevel1Connections()
      let level2 = await server.ws2pCluster.getLevel2Connections()
      const theSoftVersions: {
        software: string
        version: string
        pubkeys: string[]
      }[] = []
      Object.keys(softVersions).map((soft: any) => {
        Object.keys(softVersions[soft]).map((version: any) => {
          theSoftVersions.push({
            software: soft,
            version,
            pubkeys: softVersions[soft][version]
          })
        })
      })
      return {
        softVersions: theSoftVersions,
        level1: await Promise.all(level1.map(c => ws2pConnectionToJSON(server, c))),
        level2: await Promise.all(level2.map(c => ws2pConnectionToJSON(server, c)))
      }
    } else {
      return {
        softVersions: [],
        level1: [],
        level2: []
      }
    }
  }
}

async function ws2pConnectionToJSON(server: Server, connection: WS2PConnection|any): Promise<WS2PConnectionInfoType> {
  const pubkey = connection.pubkey
  const ws2pid = connection.uuid
  const member = await server.dal.getWrittenIdtyByPubkey(pubkey)
  if (connection.ws._socket.server) {
    return {
      pubkey: connection.pubkey,
      ws2pid: connection.uuid,
      uid: member ? member.uid : '',
      handle: connection.ws._socket.server._connectionKey.split(':').slice(1).join(':')
    }
  }
  else {
    return {
      pubkey: connection.pubkey,
      ws2pid: connection.uuid,
      uid: member ? member.uid : '',
      handle: [connection.ws._socket.remoteAddress, connection.ws._socket.remotePort].join(':')
    }
  }
}
