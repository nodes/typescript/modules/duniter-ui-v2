import {Server} from 'duniter/server'
import {NetworkConfAvailableType} from '../graphql/types/NetworkConfAvailalbeType'
import {NetworkConfDTO} from 'duniter/app/lib/dto/ConfDTO'
import {networkReconfiguration} from 'duniter/app/modules/bma'


export async function getConf(server: Server): Promise<string> {
  const raw = (await server.dal.confDAL.readRawConfFile()) || ''
  if (!raw) {
    return ''
  }
  return JSON.stringify(JSON.parse(raw), null, '\t')
}

export async function testAndSaveConf(
  server: Server,
  startServices: () => Promise<void>,
  stopServices: () => Promise<void>,
  conf: string
): Promise<string> {
  // Test the syntax
  const jsonConf = JSON.parse(conf)
  // OK: stop services and reload the server
  await stopServices()
  await server.dal.confDAL.saveConf(jsonConf)
  await server.reloadConf()
  await startServices()
  // We reuse the getConf() function
  return getConf(server)
}

export async function getNetworkConfigurations(server: Server): Promise<NetworkConfAvailableType> {
  const netconf: NetworkConfDTO = {} as any
  await new Promise((resolve, reject) => {
    networkReconfiguration(netconf, true, server.logger, false, (err: any) => {
      if (err) return reject(err)
      resolve()
    })
  })
  return netconf
}
