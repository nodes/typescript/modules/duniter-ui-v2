import {Server} from 'duniter/server'

export function gqlCurrent(server: Server) {
  return () => server.dal.getCurrentBlockOrNull()
}