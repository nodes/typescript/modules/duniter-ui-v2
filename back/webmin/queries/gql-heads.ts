import {Server} from 'duniter/server'

export function gqlHeads(server: Server) {
  return () => {
    if (server.ws2pCluster) {
      return server.ws2pCluster.getKnownHeads()
    }
    return []
  }
}
