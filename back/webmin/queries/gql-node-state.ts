import {Server} from 'duniter/server'
import {getSyncPromise} from './gql-synchronize'

let started = false

export function gqlNodeStart(startServices: () => Promise<void>) {
  return async () => {
    await startServices()
    return started = true
  }
}

export function gqlStopAndResetData(server: Server, stopServices: () => Promise<void>) {
  return async () => {
    await stopServices()
    started = false

    // Reset all the node's data
    await server.dal.close()
    await server.resetData()
    await server.dal.init(server.conf)

    return true
  }
}

export function gqlNodeState(server: Server) {
  return async () => {
    const syncPromise = getSyncPromise() // Binding to sync GQL module
    const current = await server.dal.getCurrentBlockOrNull()
    if (syncPromise.isFulfilled() && current && !started) {
      return 'READY_FOR_START'
    }
    else if (syncPromise.isFulfilled() && current) {
      return 'STARTED'
    }
    else if (!current) {
      return 'READY_FOR_SYNC'
    }
    else if (!syncPromise.isFulfilled()) {
      return 'SYNCHRONIZING'
    }
    else {
      return 'UNKNOWN'
    }
  }
}
