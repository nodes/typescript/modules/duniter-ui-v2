import Vue from "vue";
import _Vue from "vue";
import {WebminService} from '@/lib/services/webmin.service'
import ApolloClient from 'apollo-client';

/**
 * Plugin declaration for Vue Typescript compilation
 */
declare module 'vue/types/vue' {
  interface Vue {
    $webmin: WebminService
  }
}

/**
 * Plugin installer
 */
function WebminPlugin(v: typeof _Vue, options?: { $apollo: ApolloClient<any> }): void {
  // Our webmin service
  Vue.prototype.$webmin = new WebminService(() => options.$apollo)
}

/**
 * Export plugin function
 */
export default function webmin($apollo: ApolloClient<any>) {
  Vue.use(WebminPlugin, { $apollo })
  return Vue.prototype.$webmin
}
