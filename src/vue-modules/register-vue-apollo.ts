import Vue from 'vue'
import VueApollo from 'vue-apollo'
import {environment} from '@/config/environment'
import { HttpLink } from 'apollo-link-http'
import { WebSocketLink } from 'apollo-link-ws'
import { ApolloClient } from 'apollo-client'
import { InMemoryCache } from 'apollo-cache-inmemory'
import { ApolloLink } from 'apollo-link'
import {getOperationAST} from 'graphql'

Vue.use(VueApollo)

const httpLink = new HttpLink({
  uri: environment.graphql.http
});

const wsLink = new WebSocketLink({
  uri: environment.graphql.ws,
  options: {
    reconnect: true
  }
});

const cache = new InMemoryCache()

// Create the apollo client
const apolloClient = new ApolloClient({
  link: ApolloLink.split(
    // 3
    operation => {
      const operationAST = getOperationAST(operation.query, operation.operationName);
      return !!operationAST && operationAST.operation === 'subscription';
    },
    wsLink,
    httpLink,
  ),
  cache,
  defaultOptions: {
    watchQuery: {
      fetchPolicy: 'network-only',
      errorPolicy: 'all',
    },
    query: {
      fetchPolicy: 'network-only',
      errorPolicy: 'all',
    },
  }
})

export default new VueApollo({
  defaultClient: apolloClient,
})
