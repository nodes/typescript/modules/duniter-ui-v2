import Vue from 'vue'
import {FontAwesomeIcon} from '@fortawesome/vue-fontawesome'
import {library} from '@fortawesome/fontawesome-svg-core'
import * as fonts from '@fortawesome/free-solid-svg-icons'

Object
  .keys(fonts).map(k => fonts[k])
  .filter(fa => fa.icon)
  .forEach(fa => library.add(fa))

Vue.component('font-awesome-icon', FontAwesomeIcon)
