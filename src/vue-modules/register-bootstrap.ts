import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

import Vue from 'vue'
import BootstrapVue from "bootstrap-vue"
// ------------- Lighter boostrap way -------------

// Vue.component('b-button', BButton)
// Vue.component('b-badge', BBadge)
// Vue.component('b-spinner', BSpinner)
// ------------------------------------------------

declare module 'vue/types/vue' {
  interface Vue {
    $bvToast: any
  }
}

Vue.use(BootstrapVue)
