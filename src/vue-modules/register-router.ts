import Vue from 'vue'
import Router from 'vue-router'
import {RouteNames} from '@/lib/constants'
import Home from '@/views/Home.vue'
import Loader from '@/views/Loader.vue'
import Sync from '@/views/Sync.vue'
import Overview from '@/views/home/Overview.vue'
import Parameters from '@/views/home/Parameters.vue'
import Conf from '@/views/home/parameters/Conf.vue'
import Network from '@/views/home/parameters/Network.vue'
import Forkview from '@/views/home/Forkview.vue'
import Language from '@/views/home/parameters/Language.vue'
import Data from '@/views/home/Data.vue'
import HistoryOfMember from '@/views/home/data/HistoryOfMember.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: RouteNames.LOADER,
      component: Loader
    },
    {
      path: '/sync',
      name: RouteNames.SYNC,
      component: Sync
    },
    {
      path: '/home',
      name: RouteNames.HOME,
      component: Home,
      children: [{
        path: '',
        name: 'overview',
        component: Overview
      },{
        path: 'data',
        name: 'data',
        component: Data,
        children: [{
          path: '',
          name: 'forkview',
          component: Forkview,
        },{
          path: 'history-of-member',
          name: 'history',
          component: HistoryOfMember,
        }]
      },{
        path: 'parameters',
        name: 'parameters',
        component: Parameters,
        children: [{
          path: '',
          name: 'conf',
          component: Conf
        },{
          path: 'network',
          name: 'network',
          component: Network
        },{
          path: 'lang',
          name: 'lang',
          component: Language
        }]
      }]
    },
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ '@/views/About.vue')
    }
  ]
})
