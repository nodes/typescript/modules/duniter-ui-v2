import Vue from 'vue'
import VueI18n from 'vue-i18n'
import {i18nLangEn} from '@/lib/i18n/en'
import {i18nLangFr} from '@/lib/i18n/fr'
import {Constants} from '@/lib/constants'

export const i18nMessages = {
  en: (i18nLangEn as any),
  fr: (i18nLangFr as any),
}

let browserLanguage = navigator.language || (navigator as any).userLanguage

if (browserLanguage) {
  if (browserLanguage.match(/fr-FR/)) {
    browserLanguage = 'fr'
  }
  else if (browserLanguage.match(/en-.*/)) {
    browserLanguage = 'en'
  }
  else {
    browserLanguage = Constants.DEFAULT_LANGUAGE
  }
}

Vue.use(VueI18n)

export default new VueI18n({
  locale: localStorage.getItem(Constants.STORAGE_LANGUAGE) || browserLanguage,
  fallbackLocale: Constants.FALLBACK_LANGUAGE,
  messages: i18nMessages
})