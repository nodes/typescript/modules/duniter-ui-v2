import Vue from 'vue'
import Vuex, {Store} from 'vuex'
import {NodeState} from '../../common/types'
import {WebminService} from '@/lib/services/webmin.service'

Vue.use(Vuex)

interface DuniterNodeState {
  lastState: NodeState|undefined
}

export default function($webmin: WebminService): Store<DuniterNodeState> {
  return new Vuex.Store<DuniterNodeState>({
    state: {
      lastState: undefined
    },
    mutations: {
      setLastState(state, newValue: NodeState) {
        state.lastState = newValue
      }
    },
    actions: {
      async resetState({ commit }) {
        commit('setLastState', await $webmin.nodeState())

        // switch (await this.lastState) {

        // case 'STARTED':
        // case 'READY_FOR_START':
        //   await this.router.navigate(['/run'])
        //   break
        //
        // case 'READY_FOR_SYNC':
        // case 'SYNCHRONIZING':
        //   await this.router.navigate(['/sync'])
        //   break
        // }
      }
    }
  })
}
