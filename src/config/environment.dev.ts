import {Environment} from '@/config/environment'

const env: Environment = {
  production: false,
  graphql: {
    http: 'http://localhost:9220/graphql',
    ws: 'ws://localhost:9220/subscriptions'
  }
}

export default env