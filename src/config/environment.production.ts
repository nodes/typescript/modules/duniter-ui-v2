import {Environment} from '@/config/environment'

const env: Environment = {
  production: true,
  graphql: {
    http: 'http://' + location.host + '/graphql',
    ws: 'ws://' + location.host + '/subscriptions'
  }
}

export default env