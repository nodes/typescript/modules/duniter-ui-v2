import PROD from './environment.production'
import DEV from './environment.dev'

export interface Environment {
  production: boolean,
  graphql: {
    http: string
    ws: string
  }
}

export const environment = process.env.NODE_ENV === 'production' ? PROD : DEV
