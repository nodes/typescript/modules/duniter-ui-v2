export const Constants = {
  API_URL: process.env.VUE_APP_REMOTE,
  STORAGE_LANGUAGE: 'LANG',
  DEFAULT_LANGUAGE: 'en',
  FALLBACK_LANGUAGE: 'en',
}

export const RouteNames = {
  LOADER: 'loader',
  SYNC: 'sync',
  HOME: 'home',
}
