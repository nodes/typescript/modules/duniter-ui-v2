import gql from 'graphql-tag'
import ApolloClient from 'apollo-client';
import {DBBlock, NodeState, WS2PConnectionInfos, WS2PHead} from '../../../common/types'
import {HistoryOfMemberType} from '../../../back/webmin/graphql/types/HistoryOfMemberType'
import {ChainType} from '../../../back/webmin/graphql/types/ChainType'
import {NetworkConfAvailableType} from '../../../back/webmin/graphql/types/NetworkConfAvailalbeType'

export class WebminService {

  constructor(
    private getApollo: () => ApolloClient<any>
  ) {
  }

  hello() {
    return 'hello from webmin'
  }

  async current(): Promise<DBBlock> {
    const res = await this.getApollo()
    .watchQuery<{ current: DBBlock }>({
      query: gql`
          query {
              current {
                  number
                  hash
                  membersCount
                  issuersCount
                  issuer
                  monetaryMass
                  medianTime
                  time
              }
          }
      `,
    })
    .result()
    return res.data.current
  }

  async heads(): Promise<WS2PHead[]> {
    const res = await this.getApollo()
    .watchQuery<{ heads: any[] }>({
      query: gql`
          query {
              heads {
                  message
                  messageV2
                  sig
                  sigV2
                  step
              }
          }
      `,
    })
    .result()
    return res.data.heads
  }

  async ws2pInfos(): Promise<WS2PConnectionInfos> {
    const res = await this.getApollo()
    .watchQuery<{ ws2pinfos: WS2PConnectionInfos }>({
      query: gql`
          query {
              ws2pinfos {
                  softVersions {
                      pubkeys
                      software
                      version
                  }
                  level1 {
                      pubkey
                      handle
                      uid
                      ws2pid
                  }
                  level2 {
                      pubkey
                      handle
                      uid
                      ws2pid
                  }
              }
          }
      `,
    })
    .result()
    return res.data.ws2pinfos
  }

  async nodeState(): Promise<NodeState> {
    const res = await this.getApollo()
    .watchQuery<{ nodeState: NodeState }>({
      query: gql`
          query {
              nodeState
          }
      `,
    })
    .result()
    return res.data.nodeState
  }

  syncProgress() {
    return this.getApollo()
    .subscribe({
      query: gql`
          subscription {
              syncProgress {
                  applied
                  download
                  peersSync
                  sandbox
                  saved
                  sync
                  error
                  p2pData {
                      name
                      data {
                          chunkIndex
                          node {
                              failures
                              isExcluded
                              nbSuccess
                              reserved
                              responseTimes
                              p {
                                  pubkey
                                  endpoints
                              }
                              hostName
                              hasAvailableApi
                              apiName
                              avgResponseTime
                          }
                          nodes {
                              failures
                              isExcluded
                              nbSuccess
                              reserved
                              responseTimes
                              p {
                                  pubkey
                                  endpoints
                              }
                              hostName
                              hasAvailableApi
                              apiName
                              avgResponseTime
                          }
                      }
                  }
              }
          }
      `,
    })
  }

  newDocuments() {
    return this.getApollo()
    .subscribe({
      query: gql`
          subscription {
              newDocuments {
                  blocks {
                      number
                      hash
                  }
                  transactions {
                      issuers
                      output_amount
                      comment
                  }
                  identities {
                      pubkey
                      uid
                      hash
                  }
                  certifications {
                      from
                      to
                      target
                  }
                  memberships {
                      issuer
                      block
                  }
                  peers {
                      pubkey
                      endpoints
                  }
                  ws2pHeads {
                      message
                      messageV2
                      sig
                      sigV2
                      step
                  }
                  ws2pConnections {
                      host
                      port
                      pub
                  }
                  ws2pDisconnections {
                      pub
                  }
              }
          }
      `,
    })
  }

  bcEvents() {
    return this.getApollo()
    .subscribe({
      query: gql`
          subscription {
              bcEvents {
                  bcEvent
                  block {
                      number
                      hash
                  }
              }
          }
      `,
    })
  }

  async isSyncStarted() {
    const res = await this.getApollo()
    .watchQuery<{ isSyncStarted: boolean }>({
      query: gql`
          query {
              isSyncStarted
          }
      `,
    })
    .result()
    return res.data.isSyncStarted
  }

  async synchronize(node: string) {
    await this.getApollo()
    .watchQuery({
      query: gql`
          query Sync($node: String!){
              synchronize(url: $node)
          }
      `,
      variables: {
        node
      },
    })
    .result()
  }

  async stopAndResetData() {
    await this.getApollo()
    .watchQuery({
      query: gql`
          query {
              stopAndResetData
          }
      `,
    })
    .result()
  }

  async startNode() {
    await this.getApollo()
    .watchQuery({
      query: gql`
          query {
              startNode
          }
      `,
    })
    .result()
  }

  async getConf(): Promise<string> {
    const res = await this.getApollo()
    .watchQuery<{ getConf: string }>({
      query: gql`
          query {
              getConf
          }
      `,
    })
    .result()
    return res.data.getConf
  }

  async testAndSaveConf(conf: string): Promise<string> {
    const res = await this.getApollo()
    .watchQuery<{ testAndSaveConf: string }>({
      query: gql`
          query ($conf: String!) {
              testAndSaveConf(conf: $conf)
          }
      `,
      variables: {
        conf
      }
    })
    // if (await res.getLastError()) {
    //   throw Error(res.getLastError().stack)
    // }
    const result = (await res.result())
    if (result.errors && result.errors.length) {
      throw result.errors[0]
    }
    return result.data.testAndSaveConf
  }

  async uid(pub: string) {
    const res = await this.getApollo()
    .watchQuery({
      query: gql`
          query ($pub: String!){
              uid(pub: $pub)
          }
      `,
      variables: {
        pub
      },
    })
    .result()
    return res.data.uid
  }

  async getMainChain(start: number, end: number): Promise<ChainType> {
    const res = await this.getApollo()
    .watchQuery<{ getMainChain: ChainType }>({
      query: gql`
          query ($start: Int!, $end: Int!){
              getMainChain(start: $start, end: $end) {
                  blocks {
                      number
                      hash
                  }
              }
          }
      `,
      variables: {
        start,
        end
      }
    })
    .result()
    return res.data.getMainChain
  }

  async historyOfMember(pub: string) {
    const res = await this.getApollo()
    .watchQuery<{ historyOfMember: HistoryOfMemberType }>({
      query: gql`
          query ($pub: String!){
              historyOfMember(pub: $pub) {
                  stories
              }
          }
      `,
      variables: {
        pub
      },
    })
    .result()
    return res.data.historyOfMember
  }

  async getForks(start: number, end: number): Promise<ChainType> {
    const res = await this.getApollo()
    .watchQuery<{ getForks: ChainType }>({
      query: gql`
          query ($start: Int!, $end: Int!){
              getForks(start: $start, end: $end)  {
                  blocks {
                      number
                      hash
                      previousHash
                  }
              }
          }
      `,
      variables: {
        start,
        end
      }
    })
    .result()
    return res.data.getForks
  }

  async getAvailableNetworkConf() {
    const res = await this.getApollo()
    .watchQuery<{ getAvailableNetworkConf: NetworkConfAvailableType }>({
      query: gql`
          query {
              getAvailableNetworkConf {
                  remoteport
                  remotehost
                  remoteipv4
                  remoteipv6
                  port
                  ipv4
                  ipv6
                  upnp
              }
          }
      `,
    })
    .result()
    return res.data.getAvailableNetworkConf
  }
}
