export function blockstamp(b: { number: number, hash: string }) {
  return [b.number, b.hash].join('-')
}