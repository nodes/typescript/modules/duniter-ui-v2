import Vue from 'vue'
import App from '@/App.vue'
import '@/vue-modules/register-service-worker'
import '@/vue-modules/register-vue-resource'
import "@/vue-modules/register-bootstrap"
import "@/vue-modules/register-icons"
import webmin from "@/vue-modules/register-webmin"
import apolloProvider from "@/vue-modules/register-vue-apollo"
import router from '@/vue-modules/register-router'
import i18n from '@/vue-modules/register-i18n'
import store from '@/vue-modules/register-store'

Vue.config.productionTip = false

Vue.config.errorHandler = (err, vm, info) => {

  // Log error in console
  console.error(err)

  vm.$bvToast.toast(`${err.message}`, {
    title: 'Error',
    variant: 'danger',
    autoHideDelay: 8000,
    appendToast: true,
  })
}

// Depends on apollo
const $webmin = webmin(apolloProvider.defaultClient)

new Vue({
  router,
  store: store($webmin),
  i18n,
  apolloProvider,
  render: h => h(App)
}).$mount('#app')
