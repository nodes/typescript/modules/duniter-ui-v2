# Duniter UI v2

## Project setup

### Install dependencies

    yarn

### Run in dev mode

This will launch:
 
* a Duniter server with UI backend plugged as a module
* a web server with the UI in development mode

    yarn dev

You can also launch only the back or the UI, independantly:

    yarn dev:back
    yarn dev:ui